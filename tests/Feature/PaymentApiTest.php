<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentApiTest extends TestCase
{
    private $headers = ['Accept' => 'application/json'];
    /**
     * /api/payment/validation?fiat=USD&amount=100&payment=apple-pay
     * 
     * @test
     */
    public function it_should_return_200_status_on_usd()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'USD',
            'amount' => '100',
            'payment' => 'apple-pay'
        ]), $this->headers);

        $response->assertStatus(200);
    }

    /**
     * /api/payment/validation?fiat=PHP&amount=10000&payment=debit-credit-card
     * 
     * @test
     */
    public function it_should_return_200_status_on_php()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'PHP',
            'amount' => '10000',
            'payment' => 'debit-credit-card'
        ]), $this->headers);

        $response->assertStatus(200);
    }

    /**
     * /api/payment/validation?fiat=EUR&amount=15000&payment=nl-bank-transfer
     * 
     * @test
     */
    public function it_should_return_422_status_on_eur()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'EUR',
            'amount' => '15000',
            'payment' => 'nl-bank-transfer'
        ]), $this->headers);

        $response->assertStatus(422);
    }

    /**
     * /api/payment/validation?amount=15000&payment=debit-creditcard&fiat=ZZZ
     * 
     * @test
     */
    public function it_should_return_422_status_on_zzz()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'ZZZ',
            'amount' => '15000',
            'payment' => 'debit-credit-card'
        ]), $this->headers);

        $response->assertStatus(422);
    }

    /**
     * /api/payment/validation?fiat=CAD&amount=25000&payment=debit-credit-card
     * 
     * @test
     */
    public function it_should_return_422_status_on_cad()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'CAD',
            'amount' => '25000',
            'payment' => 'debit-credit-card'
        ]), $this->headers);

        $response->assertStatus(422);
    }

    /**
     * /api/payment/validation?amount=1400&fiat=JPY&payment=apple-pay
     * 
     * @test
     */
    public function it_should_return_422_status_on_jpy()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'JPY',
            'amount' => '1400',
            'payment' => 'apple-pay'
        ]), $this->headers);

        $response->assertStatus(422);
    }

    /**
     * /api/payment/validation?amount=10&fiat=USD&payment=apple-pay
     * 
     * @test
     */
    public function it_should_return_422_status_on_usd()
    {
        $response = $this->getJson(route('api.payment.validate', [
            'fiat' => 'USD',
            'amount' => '10',
            'payment' => 'apple-pay'
        ]), $this->headers);

        $response->assertStatus(422);
    }
}
