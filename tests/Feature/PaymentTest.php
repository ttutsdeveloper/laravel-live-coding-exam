<?php

namespace Tests\Feature;

use App\Services\PaymentService;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class PaymentTest extends TestCase
{

    private $url = 'https://fe84038e-35de-4a99-a9d1-e9a0af5a2204.mock.pstmn.io/api/fiat';
    private $headers = [
        'x-api-key' => 'PMAK-63a24fa59ef25453a51c210b-b4e89f19066ff790c7821925c1c659cdb2'
    ];
    /**
     * @test
     */
    public function it_should_fetch_payments_data()
    {
        $response = Http::withHeaders($this->headers)->get($this->url);

        $this->assertEquals(200, $response->getStatusCode() );
    }

    /**
     * @test
     */
    public function it_should_return_fiat_item_if_it_existing()
    {

        $fiat = 'USD';
        $amount = '100';
        $payment = 'apple-pay';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $fiat_item = $paymentService->getFiat();

        $this->assertArrayHasKey('id', $fiat_item);
        $this->assertArrayHasKey('paymentLimits', $fiat_item);
        $this->assertEquals($fiat, $fiat_item['id']);
    }

    /**
     * @test
     */
    public function it_should_return_null_if_the_fiat_item_is_not_existing()
    {
        $fiat = 'USDs';
        $amount = '100';
        $payment = 'apple-pay';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $this->assertEquals(null, $paymentService->getFiat());
        
    }

    /**
     * @test
     */
    public function it_should_return_payment_item_if_it_existing()
    {
        $fiat = 'PHP';
        $amount = '10000';
        $payment = 'debit-credit-card';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $fiat_item = $paymentService->getFiat();
        $payment_limit_item = $paymentService->getPaymentItem($fiat_item['paymentLimits']);

        $this->assertArrayHasKey('id', $payment_limit_item);
        $this->assertArrayHasKey('min', $payment_limit_item);
        $this->assertArrayHasKey('max', $payment_limit_item);
        
    }

    /**
     * @test
     */
    public function it_should_return_null_if_the_payment_list_item_is_not_existing()
    {
        $fiat = 'PHP';
        $amount = '10000';
        $payment = 'debit-credit-cards';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $fiat_item = $paymentService->getFiat();
        $payment_limit_item = $paymentService->getPaymentItem($fiat_item['paymentLimits']);

       $this->assertEquals(null, $payment_limit_item);
    }

    /**
     * @test
     */
    public function it_should_accept_valid_amount_between_min_and_max()
    {
        $fiat = 'PHP';
        $amount = '10000';
        $payment = 'debit-credit-card';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $fiat_item = $paymentService->getFiat();
        $payment_limit_item = $paymentService->getPaymentItem($fiat_item['paymentLimits']);
        $validate_amount = $paymentService->validateAmount($payment_limit_item);

       $this->assertEquals(true, $validate_amount);
    }

    /**
     * @test
     */
    public function it_should_not_accept_valid_amount_between_min_and_max()
    {
        $fiat = 'PHP';
        $amount = '100001';
        $payment = 'debit-credit-card';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $fiat_item = $paymentService->getFiat();
        $payment_limit_item = $paymentService->getPaymentItem($fiat_item['paymentLimits']);
        $validate_amount = $paymentService->validateAmount($payment_limit_item);

       $this->assertEquals(false, $validate_amount);
    }

    /**
     * @test
     */
    public function it_should_should_return_true_if_payment_is_valid()
    {
        $fiat = 'PHP';
        $amount = '10000';
        $payment = 'debit-credit-card';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $is_valid_payment = $paymentService->validatePayment();

       $this->assertEquals(true, $is_valid_payment);
    }

    /**
     * @test
     */
    public function it_should_should_return_false_if_payment_is_valid()
    {
        $fiat = 'PHP_';
        $amount = '10000';
        $payment = 'debit-credit-card';

        $paymentService = new PaymentService($fiat, $amount, $payment);
        $is_valid_payment = $paymentService->validatePayment();

       $this->assertEquals(false, $is_valid_payment);
    }
}
