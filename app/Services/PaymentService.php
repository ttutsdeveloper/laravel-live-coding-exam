<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class PaymentService
{

    /** @var string */
    private $url = 'https://fe84038e-35de-4a99-a9d1-e9a0af5a2204.mock.pstmn.io/api/fiat';
    /** @var array */
    private $headers = [
        'x-api-key' => 'PMAK-63a24fa59ef25453a51c210b-b4e89f19066ff790c7821925c1c659cdb2'
    ];

    /** @var string */
    private $fiat;

    /** @var double */
    private $amount;

    /** @var string */
    private $payment;

    /**
     * Payment Service constructor.
     *
     * @param string $fiat - The payment currenct type
     * @param double $amount - The payment amount
     * @param string $payment - The payment item type
     */
    public function __construct($fiat, $amount, $payment) {
        $this->fiat = $fiat;
        $this->amount = $amount;
        $this->payment = $payment;
    }

    /** 
     * Fetch Payment List 
     * https://fe84038e-35de-4a99-a9d1-e9a0af5a2204.mock.pstmn.io/api/fiat
     *
     * @return \Illuminate\Http\Response::json
    */
    public function fetchPayments()
    {
        return Http::withHeaders($this->headers)->get($this->url)->json();
    }

    /** 
     * Validates payment
     *
     * @return bool
    */
    public function validatePayment() {
        $fiat = $this->getFiat();
        if ($fiat ===  null) {
            return false;
        }

        $payment_limit = $this->getPaymentItem($fiat['paymentLimits']);
        if ($payment_limit === null) {
            return false;
        }

        $validate_amount = $this->validateAmount($payment_limit);
        if (!$validate_amount) {
            return false;
        }
        
        return true;
    }

    /** 
     * Get the fiat item from the list
     *
     * @return array|null
     */
    public function getFiat() {
        $paymentData = $this->fetchPayments();
       
        $fiat_key = array_search($this->fiat, array_column($paymentData, 'id'));
        return $fiat_key !== false ? $paymentData[$fiat_key] : null;
    }

    /** 
     * Get the payment limit item from the list
     *
     * @return array|null
     */
    public function getPaymentItem($payment_limits) {
        $payment_limit_key = array_search($this->payment, array_column($payment_limits, 'id'));
        return $payment_limit_key !== false ? $payment_limits[$payment_limit_key] : null;
    }

    /** 
     * Validate the amount from the min and max
     *
     * @return bool
     */
    public function validateAmount($payment_limit_item) {
        return $payment_limit_item['min'] <= $this->amount && $payment_limit_item['max'] >= $this->amount;
    }

}