<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Services\PaymentService;

class PaymentController extends Controller
{
    /**
     * Validate payment request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(PaymentRequest $request) 
    {
        $paymentService = new PaymentService($request->fiat, $request->amount, $request->payment);
        if (!$paymentService->validatePayment()) {
            return response()->json([
                'success' => false
            ], 422);
        }
        return response()->json([
            'success' => true
        ]);
    }
}
